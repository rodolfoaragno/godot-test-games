extends Node2D

var shot_trails = []
const TRAIL_LIFE = 8

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Player_hit(from,to):
	shot_trails.append({
		"from":from,
		"to":to,
		"life": TRAIL_LIFE
	})
	# Replace with function body.
func _physics_process(delta):
	update()
func _draw():
	for trail in shot_trails:
		draw_line(trail["from"], trail["to"], Color8(255,255,255, 255 * trail["life"]/TRAIL_LIFE ))
		trail["life"] -= 1
		if trail["life"] <= 0:
			shot_trails.erase(trail)
			