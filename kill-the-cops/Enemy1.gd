extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var life = 3
var target = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func receive_damage(damage, from):
	target = from
	life -= damage
	if life <=0:
		queue_free()#dies if life less than 0
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _physics_process(delta):
	if target != Vector2():
		if abs(get_angle_to( target)) > 0.5:
			if get_angle_to( target ) > 0:
				apply_torque_impulse( 50 * get_angle_to( target ) )
			else:
				apply_torque_impulse( -50 * get_angle_to( target ) )
		
		if abs(get_angle_to( target)) < 0.8:
			print ("run!")
			if linear_velocity.length() < 400:
				 apply_impulse( Vector2(), Vector2(40,0).rotated(rotation))
		
		print (get_angle_to(target))