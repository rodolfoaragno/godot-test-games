extends KinematicBody2D

export (int) var speed = 200
var shoot_range = 5000
var velocity = Vector2()
signal hit
onready var shoot_ray = get_node("GunRay")

func get_input():
	look_at(get_global_mouse_position())
	velocity = Vector2()
	if Input.is_action_pressed('down'):
		velocity += Vector2(-1, 0)
	if Input.is_action_pressed('up'):
		velocity += Vector2(1, 0)
	if Input.is_action_pressed('left'):
		velocity += Vector2(0,-1)
	if Input.is_action_pressed('right'):
		velocity += Vector2(0,1)
	velocity = velocity.normalized().rotated(rotation) * speed
	if Input.is_action_just_pressed('shoot') and $GunTimer.is_stopped():
		shoot()
    
func shoot():
	if shoot_ray.is_colliding():
		emit_signal("hit", shoot_ray.global_position , shoot_ray.get_collision_point())
		if shoot_ray.get_collider().is_in_group("enemy"):
			var enemy = shoot_ray.get_collider()
			enemy.receive_damage(1, global_position)
			enemy.apply_impulse( (enemy.global_position - shoot_ray.get_collision_point()).rotated(PI), (shoot_ray.get_collision_point() - global_position).normalized() * 100 )
		$GunTimer.start()
	
func _physics_process(delta):
	get_input()
	move_and_slide(velocity)

